package com.exam.mapper;

import com.exam.entity.MultiQuestion;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 15:43
 * @Description: com.exam.mapper
 */
@Mapper
public interface MultiQuestionMapper {
    //添加一个选择题
    @Options(useGeneratedKeys = true,keyProperty = "questionId")
    @Insert("insert into multi_question (subject,question,answerA,answerB,answerC,answerD,rightAnswer,analysis,section,level)"+
            " values (#{subject},#{question},#{answerA},#{answerB},#{answerC},#{answerD},#{rightAnswer},#{analysis},#{section},#{level})")
    int add(MultiQuestion multiQuestion);

    //获取选择题的题目id
    //查询最后一条记录的questionId
    @Select("select questionId from multi_question order by questionId desc limit 1")
    MultiQuestion find();

    //获取选择题库里面的题目id
    @Select("select questionId from multi_question where subject=#{subject} order by rand() desc limit #{changeNumber}")
    List<Integer> getBySubject(String subject, Integer changeNumber);
}
