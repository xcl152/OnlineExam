package com.exam.mapper;

import com.exam.entity.JudgeQuestion;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/29 - 15:15
 * @Description: com.exam.mapper
 */
@Mapper
public interface JudgeQuestionMapper {
    //添加一个判断题到题库
    @Options(useGeneratedKeys = true,keyProperty ="questionId" )
    @Insert("insert into judge_question(subject,question,answer,analysis,level,section)"+
            "values(#{subject},#{question},#{answer},#{analysis},#{level},#{section})")
    int add(JudgeQuestion judgeQuestion);

    /*
     *  查询最后一条记录的questionId
     *   获取添加进去的填空题的id
     * */
    @Select("select questionId from judge_question order by questionId desc limit 1")
    JudgeQuestion get();

    //从判断题库中获取判断题的id
    @Select("select questionId from judge_question where subject=#{subject} order by rand() desc limit #{judgeNumber} ")
    List<Integer> getBySubject(String subject, Integer judgeNumber);
}
