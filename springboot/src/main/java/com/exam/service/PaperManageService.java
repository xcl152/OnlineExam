package com.exam.service;

import com.exam.entity.PaperManage;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 17:03
 * @Description: com.exam.service
 */
public interface PaperManageService {
    //添加试卷的选择题
    int add(PaperManage paperManage);
}
