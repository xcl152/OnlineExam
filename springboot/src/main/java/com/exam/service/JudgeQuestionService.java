package com.exam.service;

import com.exam.entity.JudgeQuestion;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/29 - 15:15
 * @Description: com.exam.service
 */
public interface JudgeQuestionService {
    //添加一个判断题到题库
    int add(JudgeQuestion judgeQuestion);

    //获取添加进去的判断题的id
    JudgeQuestion get();

    //从判断题库中获取判断题的id
    List<Integer> getBySubject(String subject, Integer judgeNumber);
}
