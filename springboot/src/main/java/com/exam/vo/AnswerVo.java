package com.exam.vo;

import lombok.Data;

/**
 * @author;XCL
 * @Date; 2022/11/21 - 15:06
 * @Description: com.exam.vo
 */
@Data
public class AnswerVo {

    /*题目内容*/
    private String question;
    /*科目*/
    private String subject;
    /*每题分数*/
    private String score;
    /*所属章节*/
    private String section;
    /*难度等级*/
    private String level;
    /*题目类型*/
    private String type;
}
