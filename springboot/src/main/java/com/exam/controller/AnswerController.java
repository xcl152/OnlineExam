package com.exam.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ApiResult;
import com.exam.service.AnawerService;
import com.exam.util.ApiResultHandler;
import com.exam.vo.AnswerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author;XCL
 * @Date; 2022/11/18 - 17:21
 * @Description: com.exam.controller
 */
@RestController
public class AnswerController {
    @Autowired
    private AnawerService anawerService;

    @GetMapping("/answer/{page}/{size}")
    public ApiResult getAllQuestion(@PathVariable("page") Integer page,
                                    @PathVariable("size") Integer size){
        Page<AnswerVo> answerVoPage = new Page<>(page, size);
        IPage<AnswerVo> allAnswerVo = anawerService.getAllAnswer(answerVoPage);
        System.out.println("得到的数据为"+allAnswerVo);
        return ApiResultHandler.buildApiResult(200,"查询所有题库",allAnswerVo);
    }
}
