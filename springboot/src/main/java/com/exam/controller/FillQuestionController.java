package com.exam.controller;

import com.exam.entity.ApiResult;
import com.exam.entity.FillQuestion;
import com.exam.service.FillQuestionService;
import com.exam.util.ApiResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author;XCL
 * @Date; 2022/11/28 - 10:13
 * @Description: 填空题库
 */
@RestController
public class FillQuestionController {
    @Autowired
    private FillQuestionService fillQuestionService;

    /*添加填空题库*/
    @PostMapping("/FillQuestion")
    public ApiResult addFillQuestion(@RequestBody FillQuestion fillQuestion){
        int result = fillQuestionService.add(fillQuestion);
        if(result != 0){
            return ApiResultHandler.buildApiResult(200,"添加成功",result);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",result);
    }

    /*
    *  查询最后一条记录的questionId
    *   获取添加进去的填空题的id
    * */
    @GetMapping("/getFillQuestionId")
    public ApiResult getFillQuestionId(){
        FillQuestion fillId = fillQuestionService.get();
        System.out.println("questionId:"+fillId);
        return ApiResultHandler.buildApiResult(200,"查询成功",fillId);
    }
}
