package com.exam.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ApiResult;
import com.exam.entity.ExamManage;
import com.exam.service.AddAnswerService;
import com.exam.util.ApiResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author;XCL
 * @Date; 2022/11/24 - 10:34
 * @Description: com.exam.controller
 */
@RestController
public class AddAnswerController {

    @Autowired
    private AddAnswerService addAnswerService;

    @GetMapping("/addAnswer/{page}/{size}")
    public ApiResult addAnswer(@PathVariable("page") Integer page,@PathVariable("size") Integer size){
        Page<Object> pageParam = new Page<>(page, size);
        IPage<ExamManage> manageIpage = addAnswerService.add(pageParam);
        return ApiResultHandler.buildApiResult(200,"分页查询所有试卷",manageIpage);
    }
}
