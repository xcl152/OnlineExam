package com.exam.controller;

import com.exam.entity.ApiResult;
import com.exam.entity.JudgeQuestion;
import com.exam.service.JudgeQuestionService;
import com.exam.util.ApiResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author;XCL
 * @Date; 2022/11/29 - 15:12
 * @Description: com.exam.controller
 */
@RestController
public class JudgeQuestionController {
    @Autowired
    private JudgeQuestionService judgeQuestionService;

    //添加一个判断题到题库
    @PostMapping("/judgeQuestion")
    public ApiResult addJudgeQuestion(@RequestBody JudgeQuestion judgeQuestion){
        int result = judgeQuestionService.add(judgeQuestion);
        if(result != 0){
            return ApiResultHandler.buildApiResult(200,"添加成功",result);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",result);
    }

    /*
     *  查询最后一条记录的questionId
     *   获取添加进去的判断题的id
     * */
    @GetMapping("/getJudgeQuestionId")
    public ApiResult getJudgeQuestionId(){
        JudgeQuestion judgeId = judgeQuestionService.get();
        return ApiResultHandler.buildApiResult(200,"查询成功",judgeId);
    }
}
