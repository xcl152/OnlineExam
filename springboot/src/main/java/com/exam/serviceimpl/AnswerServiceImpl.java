package com.exam.serviceimpl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.mapper.AnswerMapper;
import com.exam.service.AnawerService;
import com.exam.vo.AnswerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @author;XCL
 * @Date; 2022/11/21 - 15:20
 * @Description: com.exam.serviceimpl
 */
@Service
public class AnswerServiceImpl implements AnawerService {
    @Autowired
    private AnswerMapper answerMapper;

    //查询所有题库
    @Override
    public IPage<AnswerVo> getAllAnswer(Page<AnswerVo> answerVoPage) {
        return answerMapper.getAllAnswer(answerVoPage);
    }
}
