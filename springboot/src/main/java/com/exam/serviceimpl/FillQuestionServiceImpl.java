package com.exam.serviceimpl;

import com.exam.entity.FillQuestion;
import com.exam.mapper.FillQuestionMapper;
import com.exam.service.FillQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/28 - 10:14
 * @Description: com.exam.serviceimpl
 */
@Service
public class FillQuestionServiceImpl implements FillQuestionService {
    @Autowired
    private FillQuestionMapper fillQuestionMapper;
    //添加填空题库
    @Override
    public int add(FillQuestion fillQuestion) {
        return fillQuestionMapper.add(fillQuestion);
    }

    //获取添加进去的填空题的id
    @Override
    public FillQuestion get() {
        return fillQuestionMapper.get();
    }

    //从填空题库中获取填空题id
    @Override
    public List<Integer> getBySubject(String subject, Integer fillNumber) {
        return fillQuestionMapper.getBySubject(subject,fillNumber);
    }
}
