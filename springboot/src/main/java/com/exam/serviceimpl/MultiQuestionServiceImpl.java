package com.exam.serviceimpl;

import com.exam.entity.MultiQuestion;
import com.exam.mapper.MultiQuestionMapper;
import com.exam.service.MultiQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 15:43
 * @Description: com.exam.serviceimpl
 */
@Service
public class MultiQuestionServiceImpl implements MultiQuestionService {
    @Autowired
    private MultiQuestionMapper multiQuestionMapper;

    //添加一个选择题
    @Override
    public int add(MultiQuestion multiQuestion) {
        return multiQuestionMapper.add(multiQuestion);
    }

    //获取选择题的题目id
    @Override
    public MultiQuestion find() {
        return multiQuestionMapper.find();
    }

    //获取选择题库里面的题目id
    @Override
    public List<Integer> getBySubject(String subject, Integer changeNumber) {
        return multiQuestionMapper.getBySubject(subject,changeNumber);
    }
}
