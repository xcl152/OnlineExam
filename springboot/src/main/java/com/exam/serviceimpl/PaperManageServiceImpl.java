package com.exam.serviceimpl;

import com.exam.entity.PaperManage;
import com.exam.mapper.PaperManageMapper;
import com.exam.service.PaperManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 17:03
 * @Description: com.exam.serviceimpl
 */
@Service
public class PaperManageServiceImpl implements PaperManageService {
    @Autowired
    private PaperManageMapper paperManageMapper;

    //添加试卷的一个选择题
    @Override
    public int add(PaperManage paperManage) {
        return paperManageMapper.add(paperManage);
    }
}
